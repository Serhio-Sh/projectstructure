﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Repositories;
using WebAPI.Proj.DAL.Entities;

namespace WebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private TaskRepository TaskRepo = new();

        [HttpGet]
        public ActionResult<ICollection<Task>> Get()
        {
            return Ok(TaskRepo.Read());
        }

        [HttpPost]
        public void Post([FromBody] Task dto)
        {
            TaskRepo.Create(dto);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Task task)
        {
            TaskRepo.Update(id, task);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            TaskRepo.Delete(id);
        }
    }
}
