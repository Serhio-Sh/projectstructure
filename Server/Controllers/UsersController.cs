﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Repositories;
using WebAPI.Proj.DAL.Entities;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private UserRepository UsersRepo = new();
 
        [HttpGet]
        public ActionResult<ICollection<User>> Get()
        {
            return Ok(UsersRepo.Read());
        }

        [HttpPost]
        public void Post([FromBody] User dto)
        {
            UsersRepo.Create(dto);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] User user)
        {
            UsersRepo.Update(id, user);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            UsersRepo.Delete(id);
        }
    }
}
