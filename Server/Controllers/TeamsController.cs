﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Repositories;
using WebAPI.Proj.DAL.Entities;

namespace WebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private TeamRepository TeamRepo = new();

        [HttpGet]
        public ActionResult<ICollection<Team>> Get()
        {
            return Ok(TeamRepo.Read());
        }

        [HttpPost]
        public void Post([FromBody] Team dto)
        {
            TeamRepo.Create(dto);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Team team)
        {
            TeamRepo.Update(id, team);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            TeamRepo.Delete(id);
        }
    }
}
