﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Repositories;
using WebAPI.Proj.DAL.Entities;

namespace WebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private ProjectRepository ProjRepo = new();

        [HttpGet]
        public ActionResult<ICollection<Project>> Get()
        {
            return Ok(ProjRepo.Read());
        }

        [HttpPost]
        public void Post([FromBody] Project dto)
        {
            ProjRepo.Create(dto);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Project project)
        {
            ProjRepo.Update(id, project);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
             ProjRepo.Delete(id);
        }
    }
}
