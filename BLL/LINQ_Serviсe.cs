﻿using System;
using System.Linq;
using System.Collections.Generic;
using WebAPI.Proj.Common.DTO;
using WebAPI.Proj.DAL.Repositories;
using WebAPI.Proj.DAL.Entities;


namespace WebAPI.Proj.BLL
{
    public class LINQ_Serviсe
    {
        static ProjectRepository Projects = new();
        static TeamRepository Teams = new();
        static TaskRepository Tasks = new();
        static UserRepository Users = new();
        private IEnumerable<ProjectDTO> ProjectInfo = GetStruct();

        //Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        public Dictionary<ProjectDTO, int> GetNamberTasks(int id)
        {
            var v = from c in ProjectInfo
                    select new
                    {
                        Project = c,
                        NumbTasks = c.Tasks.Count(x => x.Performer.Id == id)
                    };

            return v.ToDictionary(x => x.Project, x => x.NumbTasks);
        }

        //Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков).
        public  List<TaskDTO> GetTasks(int id)
        {
            var v = from c in ProjectInfo
                    from t in c.Tasks
                    where t.Performer.Id == id && t.Name.Length < 45
                    select t;

            return v.ToList();
        }

        //Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользова
        public Dictionary<int, string> GetFinished21(int id)
        {
            var v = from c in ProjectInfo
                    from t in c.Tasks
                    where t.Performer.Id == id && t.FinishedAt.Year == 2021
                    select new { t.Id, t.Name };

            return v.ToDictionary(x => x.Id, x => x.Name);
        }

        //Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, отсортированных по дате
        //регистрации пользователя по убыванию, а также сгруппированных по командам.
        public  Dictionary<Team, List<User>> GetTeam_Users()
        {
            var v = from c in ProjectInfo
                    let performers = c.Tasks.Select(x => x.Performer).Distinct()
                    where (performers.All(u => 2021 - u.BirthDay.Year > 10))
                    select new
                    {
                        Team = c.Team,
                        Users = performers.OrderByDescending(x => x.RegisteredAt).ToList()
                    };

            return v.ToDictionary(x => x.Team, x => x.Users);
        }

        //Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        public Dictionary<User, List<Task>> GetUser_Tasks()
        {
            var v = from t in Tasks.Read()
                    orderby t.Name.Length descending
                    group t by t.PerformerId into g
                    join u in Users.Read() on g.Key equals u.Id
                    orderby u.FirstName
                    select new
                    {
                        User = u,
                        Tasks = g.ToList()
                    };

            return v.ToDictionary(x => x.User, x => x.Tasks);
        }



        private static IEnumerable<ProjectDTO> GetStruct()
        {
            return from proj in Projects.Read()
                   join autor in Users.Read() on proj.AuthorId equals autor.Id
                   join team in Teams.Read() on proj.TeamId equals team.Id
                   select new ProjectDTO
                   {
                       Name = proj.Name,
                       Description = proj.Description,
                       Deadline = proj.Deadline,
                       CreatedAt = proj.Deadline,
                       Tasks = from task in Tasks.Read()
                               where proj.Id == task.ProjectId
                               join u in Users.Read() on task.PerformerId equals u.Id
                               select new TaskDTO
                               {
                                   Performer = u,
                                   Id = task.Id,
                                   PerformerId = task.PerformerId,
                                   Name = task.Name,
                                   Description = task.Description,
                                   State = task.State,
                                   CreatedAt = task.CreatedAt,
                                   FinishedAt = task.FinishedAt
                               },
                       Autor = autor,
                       Team = team
                   };
        }
    }
}
