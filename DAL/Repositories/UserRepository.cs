﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using WebAPI.Proj.DAL.Entities;
using WebServer.Repositories.Interface;


namespace WebAPI.Proj.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        static string JsonString = File.ReadAllText($@"{Directory.GetCurrentDirectory()}\Data\Users.json");
        static List<User> Entities { get; set; } = JsonConvert.DeserializeObject<List<User>>(JsonString);


        public void Create(User user)
        {
            Entities.Add(user);
        }

        public IEnumerable<User> Read()
        {
            return Entities;
        }

        public void Update(int id, User new_user)
        {
            Entities[id] = new_user;
        }

        public void Delete(int id)
        {
            Entities = Entities.Where(x => x.Id != id).ToList();
        }
    }
}
