﻿using System;
using System.Collections.Generic;
using WebAPI.Proj.DAL.Entities.Interface;

namespace WebServer.Repositories.Interface
{
    interface IRepository<T> where T: IEntity
    {
        static string JsonString;
        static List<T> Entities { get; set; }

         void Create(T entity);

         IEnumerable<T> Read();

         void Update(int id, T new_entity);

         void Delete(int id);
    }
}
