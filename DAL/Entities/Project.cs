﻿using Newtonsoft.Json;
using WebAPI.Proj.DAL.Entities.Interface;


namespace WebAPI.Proj.DAL.Entities
{
    public class Project : IEntity
    {
        public int Id { get; set; }
        public int AuthorId;
        public int TeamId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }
        public string Deadline { get; set; }
        public string CreatedAt { get; set; }
    }
}
