﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using WebAPI.Proj.Common.DTO;
using WebAPI.Proj.DAL.Entities;

namespace Client
{
    class Program
    {
        private const string APP_PATH = "https://localhost:5001";
        static readonly HttpClient client = new();

        static void Main(string[] args)
        {   var response = client.GetStringAsync(APP_PATH + "/api/Projects").Result;
            var projects = JsonConvert.DeserializeObject<List<Project>>(response);

            response = client.GetStringAsync(APP_PATH + "/api/Tasks").Result;
            var tasks = JsonConvert.DeserializeObject<List<Task>>(response);

            response = client.GetStringAsync(APP_PATH + "/api/Teams").Result;
            var teams = JsonConvert.DeserializeObject<List<Team>>(response);

            response = client.GetStringAsync(APP_PATH + "/api/Users").Result;
            var users = JsonConvert.DeserializeObject<List<User>>(response);


            var Projects = from proj in projects
                           join autor in users on proj.AuthorId equals autor.Id
                           join team in teams on proj.TeamId equals team.Id
                           select new ProjectDTO
                           {
                               Name = proj.Name,
                               Description = proj.Description,
                               Deadline = proj.Deadline,
                               CreatedAt = proj.Deadline,
                               Tasks = from task in tasks
                                       where proj.Id == task.ProjectId
                                       join u in users on task.PerformerId equals u.Id
                                       select new TaskDTO
                                       {
                                           Performer = u,
                                           Id = task.Id,
                                           PerformerId = task.PerformerId,
                                           Name = task.Name,
                                           Description = task.Description,
                                           State = task.State,
                                           CreatedAt = task.CreatedAt,
                                           FinishedAt = task.FinishedAt
                                       },
                               Autor = autor,
                               Team = team
                           };
        }


        //Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        public static Dictionary<ProjectDTO, int> GetNamberTasks<T>(int id, IEnumerable<ProjectDTO> collection)
        {
            var v = from c in collection
                    select new
                    {
                        Project = c,
                        NumbTasks = c.Tasks.Count(x => x.Performer.Id == id)
                    };

            return v.ToDictionary(x => x.Project, x => x.NumbTasks);
        }

        //Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков).
        public static List<TaskDTO> GetTasks(int id, IEnumerable<ProjectDTO> collection)
        {
            var v = from c in collection
                    from t in c.Tasks
                    where t.Performer.Id == id && t.Name.Length < 45
                    select t;

            return v.ToList();
        }

        //Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользова
        public static Dictionary<int, string> GetFinished21(int id, IEnumerable<ProjectDTO> collection)
        {
            var v = from c in collection
                    from t in c.Tasks
                    where t.Performer.Id == id && t.FinishedAt.Year == 2021
                    select new { t.Id, t.Name };

            return v.ToDictionary(x => x.Id, x => x.Name);
        }

        //Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, отсортированных по дате
        //регистрации пользователя по убыванию, а также сгруппированных по командам.
        public static Dictionary<Team, List<User>> GetTeam_Users(IEnumerable<ProjectDTO> collection)
        {
            var v = from c in collection
                    let performers = c.Tasks.Select(x => x.Performer).Distinct()
                    where (performers.All(u => 2021 - u.BirthDay.Year > 10))
                    select new
                    {
                        Team = c.Team,
                        Users = performers.OrderByDescending(x => x.RegisteredAt).ToList()
                    };

            return v.ToDictionary(x => x.Team, x => x.Users);
        }

        //Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        public static Dictionary<UserDTO, List<Task>> GetUser_Tasks(IEnumerable<UserDTO> users, IEnumerable<Task> tasks)
        {
            var v = from t in tasks
                    orderby t.Name.Length descending
                    group t by t.PerformerId into g
                    join u in users on g.Key equals u.Id
                    orderby u.FirstName
                    select new
                    {
                        User = u,
                        Tasks = g.ToList()
                    };

            return v.ToDictionary(x => x.User, x => x.Tasks);
        }

        // 7.
        public static List<Proj> GetProjects(IEnumerable<ProjectDTO> collection)
        {
            var v = from c in collection
                    let ollUsers = c.Tasks.Where(t => t.PerformerId != c.Autor.Id).Select(x => x.Performer).Distinct()
                    let maxDisc = c.Tasks.First(t => t.Name.Length == c.Tasks.Max(x => x.Description.Length))
                    let minName = c.Tasks.First(t => t.Name.Length == c.Tasks.Min(x => x.Name.Length))
                    select new Proj
                    {
                        _Project = c,
                        LongestTask = maxDisc,
                        ShortTask = minName,
                        CountUsers = (c.Description.Length > 20 || c.Tasks.Count() < 3)
                                     ? (ollUsers.Count() + 1)// +Autor
                                     : (0)
                    };

            return v.ToList();
        }

        public class Proj
        {
            public ProjectDTO _Project { get; set; }
            public TaskDTO LongestTask { get; set; }
            public TaskDTO ShortTask { get; set; }
            public int CountUsers { get; set; }
        }
    }
}
